NAME = philo_visualizer
CC = cc
CFLAGS = -Wall -Werror -Wextra -D BUFFER_SIZE=300
MLXDIR = ./MLX42
MLX = ./MLX42/libmlx42.a

SRCS =	./srcs/main.c							\
		./srcs/get_philo_state.c				\
		./srcs/result_to_struct.c				\
		./srcs/utilities.c						\
		./srcs/visualize.c						\
		./srcs/debugging_tools.c				\
		./srcs/error_exit.c						\
		./get_next_line/get_next_line.c			\
		./get_next_line/get_next_line_utils.c	\

OBJS = $(SRCS:.c=.o)

all: mac

linux: $(OBJS) $(MLX)
	@$(CC) $(OBJS) $(MLX) -ldl -lglfw -o $(NAME)
	@make clean

mac: $(OBJS) $(MLX)
	@$(CC) $(OBJS) $(MLX) MLX42/libglfw3.a -framework Cocoa -framework OpenGL -framework IOKit -o $(NAME)
	@make clean

sanitize: fclean $(OBJS) $(MLX) #ONLY LINUX
	@$(CC) $(OBJS) $(MLX) -ldl -lglfw -o $(NAME) -g -fsanitize=address
	@make clean

$(MLX):
	@make -C ./MLX42

clean:
	@rm -f $(OBJS)
	@cd $(MLXDIR)/ && make clean

fclean: clean
	@rm -f $(NAME)
	@rm -f $(MLX)

re:
	@make fclean
	@make all