/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_visualizer.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/09 15:11:07 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/14 17:37:13 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_VISUALIZER_H
# define PHILO_VISUALIZER_H

# include "../get_next_line/get_next_line.h"
# include "../MLX42/include/MLX42/MLX42.h"
# include <stdio.h>
# include <fcntl.h>
# include <string.h>
# include <sys/wait.h>

extern int	window_bar;
extern int	screen_width;
extern int	philo_height;
extern char *philo_path;

typedef struct s_uni
{
	int				philo_count;
	int				lines_count;
	long			start_time;
	long			end_time;
	long double		px_per_milli;
	struct s_philo	**philos;
	// struct s_images	*images;
	mlx_t			*mlx;
	mlx_image_t		*print_img;
	mlx_image_t		*selection_img;
	mlx_image_t		*dead_img;
	mlx_image_t		*mouse_millis;
	mlx_image_t		*mouse_pos;
	mlx_image_t		*state_box;
	mlx_image_t		*state;
	mlx_image_t		*start;
	mlx_image_t		*end;
	mlx_image_t		*duration;
}					t_uni;

typedef struct	s_philo
{
	int				states_count;
	int				*state_array;
	long			*milli_array;
	mlx_image_t		*image;
	int				last_draw_state;
	long			last_draw_state_time;
}					t_philo;

// typedef struct s_images
// {
// 	mlx_image_t		*state;
// 	mlx_image_t		*start;
// 	mlx_image_t		*end;
// 	mlx_image_t		*duration;
// }					t_images;



// main
int			main(int argc, char *argv[]);
// get_philo_state
void		skip_ansi_color(int *i, char *str);
void		skip_number(int *i, char *str);
long		get_millis(char *str);
int			get_philo(char *str);
int			get_state(char *str);
// result_to_struct
void		get_philo_result(int argc, char *argv[]);
void		data_to_struct(t_uni *uni);
t_uni		*init_structs(void);
// utilities
int			get_rgba(int r, int g, int b, int a);
int			count_lines(char *file);
int			get_int_from_argv(char *file);
long		get_nbr(int *i, const char *str);
// visualize
void		visualize(t_uni *uni);
// debugging_tools
void		print_result(void);
void		print_philo_states(int philo_num);
void		select_state(t_uni *uni, int philo_num, int x);
void		draw_mouse_pos_millis(t_uni *uni, double x, double y);
// error_exit
void		free_stuff(t_uni *uni);
void		exit_visualizer(void *param);
void		error_exit(char *err_str, t_uni *uni);

#endif