/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   result_to_struct.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 18:19:38 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/28 01:37:10 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

void	get_philo_result(int argc, char *argv[])
{
	char	*args[7];
	int		file;
	int		i;

	file = open("philo_result", O_WRONLY | O_CREAT | O_TRUNC, 00777);
	if (dup2(file, STDOUT_FILENO) == -1)
		return ;
	args[0] = philo_path;
	i = 0;
	while (++i < argc)
		args[i] = argv[i];
	args[i] = NULL;
	execvp(args[0], args);
}

void	data_to_philo_struct(t_uni *uni, char *str, int line_num)
{
	int		philo_num;
	char	error_str[100];
	if (get_philo(str) == 0 || get_millis(str) == -1 || get_state(str) == -1)
	{
		snprintf(error_str, 100, "ERROR\ncould not read line %d from philo_result", line_num);
		error_exit(error_str, uni);
	}
	philo_num = get_philo(str) - 1;
	uni->philos[philo_num]->state_array[uni->philos[philo_num]
		->states_count] = get_state(str);
	uni->philos[philo_num]->milli_array[uni->philos[philo_num]
		->states_count] = get_millis(str);
	uni->philos[philo_num]->states_count++;
}

void	data_to_struct(t_uni *uni)
{
	char	*str;
	int		file;
	int		i;

	file = open("philo_result", O_RDONLY);
	if (file < 0)
		printf("ERROR\n");
	i = 0;
	while (++i)
	{
		str = get_next_line(file);
		if (!str || str[0] == '\n')
			break ;
		if (i == 1)
			uni->start_time = get_millis(str);
		data_to_philo_struct(uni, str, i);
		uni->end_time = get_millis(str);
		free (str);
	}
	uni->px_per_milli = (screen_width - 100)
		/ (long double)(uni->end_time - uni->start_time);
	close(file);
}

t_uni	*init_structs(void)
{
	t_uni			*uni;
	int				i;

	uni = calloc(1, sizeof(t_uni));
	if (!uni)
		error_exit("ERROR\nallocation failed!", NULL);
	uni->philo_count = get_int_from_argv("philo_result");
	uni->lines_count = count_lines("philo_result");
	uni->philos = calloc(uni->philo_count, sizeof(t_philo *));
	if (!uni->philos)
		error_exit("ERROR\nallocation failed!", uni);
	i = -1;
	while (++i < uni->philo_count)
	{
		uni->philos[i] = calloc(1, sizeof(t_philo));
		if (!uni->philos[i])
			error_exit("ERROR\nallocation failed!", uni);
		uni->philos[i]->states_count = 0;
		uni->philos[i]->state_array = calloc(uni->lines_count, sizeof(int));
		uni->philos[i]->milli_array = calloc(uni->lines_count, sizeof(long));
		if (!uni->philos[i]->state_array || !uni->philos[i]->milli_array)
			error_exit("ERROR\nallocation failed!", uni);
	}
	return (uni);
}
