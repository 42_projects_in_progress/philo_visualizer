/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utilities.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/09 15:14:36 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/13 14:59:12 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

int	get_rgba(int r, int g, int b, int a)
{
	return (r << 24 | g << 16 | b << 8 | a);
}

int	count_lines(char *file)
{
	int		i;
	int		file_fd;
	char	*line;

	file_fd = open(file, O_RDONLY);
	i = 1;
	line = get_next_line(file_fd);
	while (line)
	{
		free (line);
		line = get_next_line(file_fd);
		i++;
	}
	close(file_fd);
	return (i);
}

int	get_int_from_argv(char *file)
{
	int		i;
	int		file_fd;
	char	*line;

	file_fd = open(file, O_RDONLY);
	i = 0;
	line = get_next_line(file_fd);
	while (line)
	{
		if (get_philo(line) > i)
			i = get_philo(line);
		free (line);
		line = get_next_line(file_fd);
	}
	close(file_fd);
	return (i);
}

long	get_nbr(int *i, const char *str)
{
	long	nbr;

	nbr = 0;
	while (str[*i] >= '0' && str[*i] <= '9')
		nbr = nbr * 10 + (str[(*i)++] - '0');
	return (nbr);
}
