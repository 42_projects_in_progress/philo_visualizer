/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   debugging_tools.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 23:46:57 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/14 18:13:57 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

void	print_result()
{
	char	*str;
	int		file;

	file = open("philo_result", O_RDONLY);
	if (file < 0)
		printf("ERROR\n");
	while (1)
	{
		str = get_next_line(file);
		if (!str)
			break ;
		printf("%s", str);
		free (str);
	}
	printf("\n");
	close(file);
}

void	print_philo_states(int philo_num)
{
	char	*str;
	int		file;
	int		i;

	file = open("philo_result", O_RDONLY);
	if (file < 0)
		printf("ERROR\n");
	i = 0;
	printf("\e[1;31mPhilosopher %d:\e[0m\n", philo_num);
	while (++i)
	{
		str = get_next_line(file);
		if (!str)
			break ;
		if (get_philo(str) == philo_num)
			printf("%s", str);
		free (str);
	}
	printf("\n");
	close(file);
}

void	draw_selection_frame(t_uni *uni, int ph, int i)
{
	long	x;
	long	x_end;
	long	y;
	int		frame;
	int		j;

	uni->selection_img = mlx_new_image(uni->mlx, screen_width, philo_height);
	mlx_image_to_window(uni->mlx, uni->selection_img, 0, ph * philo_height);
	j = i + 1;
	while (uni->philos[ph]->milli_array[j])
	{
		if (uni->philos[ph]->state_array[j] != 1)
		{
			x_end = (uni->philos[ph]->milli_array[j] - uni->start_time) * uni->px_per_milli;
			break ;
		}
		j++;
	}
	if (!uni->philos[ph]->milli_array[j])
		x_end = (uni->end_time - uni->start_time) * uni->px_per_milli;
	frame = 2;
	y = -1;
	while (++y < philo_height)
	{
		x = (uni->philos[ph]->milli_array[i] - uni->start_time) * uni->px_per_milli;
		while (++x <= x_end)
		{
			if (x - frame <= (uni->philos[ph]->milli_array[i] - uni->start_time) * uni->px_per_milli
				|| x + frame > x_end || y < frame || y + frame >= philo_height)
				mlx_put_pixel(uni->selection_img, x - 1, y, get_rgba(255, 0, 0, 255));
		}
	}
}

void	draw_state_box(t_uni *uni, int state, long start, long end)
{
	int		box_width;
	int		box_height;
	char	state_str[100];
	char	start_str[100];
	char	end_str[100];
	char	duration_str[100];
	
	if (state == 2)
		snprintf(state_str, 100, "state:      eating");
	if (state == 3)
		snprintf(state_str, 100, "state:      sleeping");
	if (state == 4)
		snprintf(state_str, 100, "state:      thinking");
	if (state == 5)
		snprintf(state_str, 100, "state:      dead");
	snprintf(start_str, 100, "start:      %ld", start);
	snprintf(end_str, 100, "end:        %ld", end);
	snprintf(duration_str, 100, "duration:   %ld", end - start);
	box_width = 210;
	box_height = 80;
	uni->state_box = mlx_new_image(uni->mlx, box_width, box_height);
	mlx_image_to_window(uni->mlx, uni->state_box, 20 - 5, 5);
	while (--box_width > 0)
	{
		while (--box_height > 0)
			mlx_put_pixel(uni->state_box, box_width, box_height, get_rgba(50, 50, 0, 100));
		box_height = 80;
	}
	uni->state = mlx_put_string(uni->mlx, state_str, 20, 5);
	uni->start = mlx_put_string(uni->mlx, start_str, 20, 25);
	uni->end = mlx_put_string(uni->mlx, end_str, 20, 45);
	uni->duration = mlx_put_string(uni->mlx, duration_str, 20, 65);
}

void	get_selection_result(t_uni *uni, int ph, int i)
{
	int		state;
	long	start;
	long	end;

	state = uni->philos[ph]->state_array[i];
	start = uni->philos[ph]->milli_array[i];
	end = 0;
	while (end == 0 && i++ < uni->philos[ph]->states_count)
	{
		if (uni->philos[ph]->milli_array[i]
			&& uni->philos[ph]->state_array[i] != 1)
			end = uni->philos[ph]->milli_array[i];
	}
	if (!uni->philos[ph]->milli_array[i])
		end = uni->end_time;
	draw_state_box(uni, state, start, end);
}

void	select_state(t_uni *uni, int philo_num, int x)
{
	long	milli;
	int		i;

	milli = (x / uni->px_per_milli) + uni->start_time;
	i = uni->philos[philo_num - 1]->states_count;
	while (--i >= 0)
	{
		if (uni->philos[philo_num - 1]->milli_array[i] <= milli
			&& uni->philos[philo_num - 1]->state_array[i] != 1)
		{
			draw_selection_frame(uni, philo_num - 1, i);
			get_selection_result(uni, philo_num - 1, i);
			break ;
		}
	}
}

void	draw_mouse_pos_millis(t_uni *uni, double x, double y)
{
	long	pos_millis;
	char	str[20];
	long	height;

	height = uni->philo_count * philo_height;
	if(!uni->mouse_pos)
	{
		uni->mouse_pos = mlx_new_image(uni->mlx, 1, height);
		mlx_image_to_window(uni->mlx, uni->mouse_pos, x, 0);
		while (--height >= 0)
			mlx_put_pixel(uni->mouse_pos, 0, height, get_rgba(255, 0, 0, 255));
	}
	if(uni->mouse_millis)
		mlx_delete_image(uni->mlx, uni->mouse_millis);
	pos_millis = (x / uni->px_per_milli);
	snprintf(str, 20, "%ld", pos_millis);
	if (x < screen_width - 99)
	{
		uni->mouse_millis = mlx_put_string(uni->mlx, str, x + 2, y + 15);
		uni->mouse_pos->instances[0].x = (int32_t)x;
	}
}
