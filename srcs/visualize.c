/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 12:32:02 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/14 18:10:16 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

void	keyhook(mlx_key_data_t keydata, void *param)
{
	t_uni	*uni;

	uni = ((t_uni *)param);
	if (keydata.key == MLX_KEY_ESCAPE && keydata.action == MLX_PRESS)
		exit_visualizer(uni);
	if (keydata.key == MLX_KEY_P && keydata.action == MLX_PRESS)
		print_result();
}

void	mousehook(mouse_key_t button, action_t action,
			modifier_key_t mods, void *param)
{
	t_uni	*uni;
	int32_t	x;
	int32_t	y;

	uni = ((t_uni *)param);
	if (button == MLX_MOUSE_BUTTON_LEFT && action == MLX_PRESS)
	{
		if (uni->selection_img)
			mlx_delete_image(uni->mlx, uni->selection_img);
		if (uni->state_box)
			mlx_delete_image(uni->mlx, uni->state_box);
		if (uni->state)
			mlx_delete_image(uni->mlx, uni->state);
		if (uni->start)
			mlx_delete_image(uni->mlx, uni->start);
		if (uni->end)
			mlx_delete_image(uni->mlx, uni->end);
		if (uni->duration)
			mlx_delete_image(uni->mlx, uni->duration);
		mlx_get_mouse_pos(uni->mlx, &x, &y);
		if ((y / philo_height) + 1 <= uni->philo_count && x < screen_width
			&& x >= screen_width - 35)
			print_philo_states((y / philo_height) + 1);
		if ((y / philo_height) + 1 <= uni->philo_count
			&& x < screen_width - 100)
			select_state(uni, (y / philo_height) + 1, x);
	}
	(void)mods;
}

void	cursorhook(double xpos, double ypos, void *param)
{
	t_uni			*uni;
	// static long		pos_millis;

	uni = ((t_uni *)param);
	// if (pos_millis != (long)(xpos / uni->px_per_milli))
	// {
		// pos_millis = (xpos / uni->px_per_milli);
		draw_mouse_pos_millis(uni, xpos, ypos);
	// }
}

int	get_state_color(int state)
{
	if (state == 2)
		return (get_rgba(0, 204, 0, 255)); // eat
	if (state == 3)
		return (get_rgba(0, 128, 255, 255)); // sleep
	if (state == 4)
		return (get_rgba(200, 200, 50, 255)); // think
	if (state == 5)
		return (get_rgba(200, 0, 0, 255)); // dead
	return (get_rgba(0, 0, 0, 0));
}

void	draw_state_to_image(t_uni *uni, int i, long actual_state_time)
{
	long	x;
	long	y;
	long	end_x;

	if (uni->philos[i]->last_draw_state_time != 0)
		x = (uni->philos[i]->last_draw_state_time - uni->start_time) * uni->px_per_milli;
	else
		x = uni->start_time * uni->px_per_milli;
	y = 0;
	end_x = (actual_state_time - uni->start_time) * uni->px_per_milli;
	while (x < end_x)
	{
		while (y < philo_height)
		{
			if (y != 0 && y != philo_height - 1)
				mlx_put_pixel(uni->philos[i]->image, x, y,
					get_state_color(uni->philos[i]->last_draw_state));
			y++;
		}
		x++;
		y = 0;
	}
}

void	draw_states(t_uni *uni)
{
	int		state;
	int		i;
	int		j;
	char	str[5];

	i = -1;
	while (++i < uni->philo_count)
	{
		uni->philos[i]->image = mlx_new_image(uni->mlx, screen_width, philo_height);
		mlx_image_to_window(uni->mlx, uni->philos[i]->image, 0, i * philo_height);
		mlx_image_to_window(uni->mlx, uni->print_img, screen_width - 40, i * philo_height + ((philo_height - 32) / 2));
		snprintf(str, 5, "%d", i + 1);
		mlx_put_string(uni->mlx, str, 0, i * philo_height + ((philo_height - 20) / 2));
		uni->philos[i]->last_draw_state = -1;
		j = -1;
		while (++j < uni->philos[i]->states_count)
		{
			state = uni->philos[i]->state_array[j];
			if (state >= 2 && state <= 5)
			{
				draw_state_to_image(uni, i, uni->philos[i]->milli_array[j]);
				uni->philos[i]->last_draw_state = state;
				uni->philos[i]->last_draw_state_time = uni->philos[i]
					->milli_array[j];
			}
			if (uni->philos[i]->last_draw_state == 5)
			{
				mlx_image_to_window(uni->mlx, uni->dead_img,
					screen_width - 90, i * philo_height + ((philo_height - 32) / 2));
			}
		}
		draw_state_to_image(uni, i, uni->end_time);
	}
}

void	visualize(t_uni *uni)
{
	mlx_texture_t	*print_tex;
	mlx_texture_t	*dead_tex;

	mlx_set_setting(MLX_DECORATED, window_bar);
	uni->mlx = mlx_init(screen_width, uni->philo_count * philo_height,
			"42_Philosophers_visualizer", true);
	if (!uni->mlx)
		error_exit("ERROR\nfailed to initialize MLX", uni);
	print_tex = mlx_load_png("./assets/print.png");
	uni->print_img = mlx_texture_to_image(uni->mlx, print_tex);
	dead_tex = mlx_load_png("./assets/dead32.png");
	uni->dead_img = mlx_texture_to_image(uni->mlx, dead_tex);
	mlx_close_hook(uni->mlx, &exit_visualizer, uni);
	mlx_key_hook(uni->mlx, &keyhook, uni);
	mlx_mouse_hook(uni->mlx, &mousehook, uni);
	mlx_cursor_hook(uni->mlx, &cursorhook, uni);
	draw_states(uni);
	mlx_loop(uni->mlx);
}
