/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/08 21:17:08 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/28 00:19:18 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

int		window_bar = 1; 			//	1 / 0
int		screen_width = 1920;		//	> 350
int		philo_height = 30;			//	> 30
char	*philo_path = "../philo";	//	Path to your Philosophers programm.

int	main(int argc, char *argv[])
{
	int		pid;
	t_uni	*uni;

	if (argc != 1)
	{
		if (argc != 6 && argc != 5)
			error_exit("wrong number of arguments!", NULL);
		if (access(philo_path, F_OK) != 0)
			error_exit("ERROR\nyour philo does not exist at the given path...", NULL);
		pid = fork();
		if (pid == -1)
			return (1);
		if (pid == 0)
			get_philo_result(argc, argv);
		printf("your philo is running ! Please wait...\nif it needs longer than expected, abort with Ctrl+C and check the philo_result to see if your philo got stuck in a deadlock\n");
		waitpid(pid, NULL, 0);
		printf("Finished !\n");
	}
	printf("ESC\t\texit the visualizer\nP\t\tprint philo_result\n\n");
	uni = init_structs();
	data_to_struct(uni);
	visualize(uni);
	return (0);
}
