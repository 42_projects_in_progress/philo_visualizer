/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_exit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/12 10:44:03 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/12 18:33:10 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

void	free_stuff(t_uni *uni)
{
	int	i;

	if (uni->philos)
	{
		i = -1;
		while (++i < uni->philo_count)
		{
			if (uni->philos[i])
			{
				if (uni->philos[i]->state_array)
					free(uni->philos[i]->state_array);
				if (uni->philos[i]->milli_array)
					free(uni->philos[i]->milli_array);
				free(uni->philos[i]);
			}
		}
		free(uni->philos);
	}
	free(uni);
}

void	exit_visualizer(void *param)
{
	t_uni	*uni;

	uni = (t_uni *)param;
	mlx_terminate(uni->mlx);
	free_stuff(uni);
	exit(0);
}

void	error_exit(char *err_str, t_uni *uni)
{
	if (uni)
		free_stuff(uni);
	printf("%s\n", err_str);
	exit(EXIT_FAILURE);
}
