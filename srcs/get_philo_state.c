/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_philo_state.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tfriedri <tfriedri@student.42heilbronn.    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/09/09 01:17:21 by tfriedri          #+#    #+#             */
/*   Updated: 2022/09/13 02:38:04 by tfriedri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo_visualizer.h"

void	skip_ansi_color(int *i, char *str)
{
	if (str[*i] == 27)
	{
		while (str[*i] != 'm')
			(*i)++;
		(*i)++;
	}
}

void	skip_number(int *i, char *str)
{
	skip_ansi_color(i, str);
	while (str[*i] >= 48 && str[*i] <= 57)
		(*i)++;
	skip_ansi_color(i, str);
	while (str[*i] == ' ' || (str[*i] >= 9 && str[*i] <= 13))
		(*i)++;
}

long	get_millis(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
	{
		if (str[i] >= 48 && str[i] <= 57)
			return (get_nbr(&i, str));
	}
	return (-1);
}

int	get_philo(char *str)
{
	int	i;

	i = 0;
	skip_number(&i, str);
	skip_ansi_color(&i, str);
	return (get_nbr(&i, str));
}

int	get_state(char *str)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (j++ <= 2)
		skip_number(&i, str);
	skip_ansi_color(&i, str);
	if (strncmp(str + i, "has ", 4) == 0)
		return (1);
	if (strncmp(str + i, "is e", 4) == 0)
		return (2);
	if (strncmp(str + i, "is s", 4) == 0)
		return (3);
	if (strncmp(str + i, "is t", 4) == 0)
		return (4);
	if (strncmp(str + i, "died", 4) == 0)
		return (5);
	return (-1);
}